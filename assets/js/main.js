// //////////////////////////////////////////////////////////////////////////////////////////////////
// ==================================================================================================
//
// Global javascript
// * Browser condition
// * Main menu
// * Account menu
// * Notifyjs
//
// ==================================================================================================
// //////////////////////////////////////////////////////////////////////////////////////////////////


$(function() {
  // Browser condition
  // ============================================================
  if(is.ie()) {
    $('html').addClass('ie');
  } else if (is.edge()) {
    $('html').addClass('edge');
  } else if (is.mac() && is.safari()) {
    $('html').addClass('safari');
  } else if (is.firefox()) {
    $('html').addClass('firefox');
  } else if ((is.iphone() || is.ipad()) && is.safari()) {
    $('html').addClass('safari-mobile');
  } else if (is.android() && is.chrome()) {
    $('html').addClass('chrome-mobile');
  }

  // Notifyjs
  // ============================================================
  // Custom template
  $.notify.addStyle('validation', {
    html:
      "<div>" +
      // "<img class='notifyjs-icon' src='../assets/images/icons/icon-info.svg' alt='Icon' />" +
      "<span class='notifyjs-message' data-notify-text='message'></span>" +
      "</div>",
    classes: {
      base: {},
      error: {},
      success: {}
    }
  });

  // Default options
  $.notify.defaults({
    position: "top right",
    style: "validation",
    showAnimation: "fadeIn",
    showDuration: 300,
    hideAnimation: "fadeOut",
    hideDuration: 300
  });

  // Main menu
  // ============================================================
  $('#user-menu-button').on("click", function () {
    $('#user-menu').toggleClass('is-active');
  });
});


